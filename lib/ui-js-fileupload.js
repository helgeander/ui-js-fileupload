import { template } from './ui-js-lib.js';

const tpl = template`
    <form action='#' method='post' enctype="multipart/form-data">
        <input type="file" name="files" multiple hidden>
    </form>
`;

class UiJsFileupload extends HTMLElement {

  constructor() {
    super();

    this.files = [];
    this.filesInTransit = [];
  }

  async connectedCallback() {
    this.url = this.getAttribute('url');
    this.parentDrop = this.getAttribute('parent-drop') == "true";

    tpl().render(this);

    this.form = this.querySelector('form');
    this.fileInput = this.querySelector('input[type="file"]');

    this.form.addEventListener("click", () =>{
      this.selectFiles();
    });

    this.fileInput.onchange = async ({target}) => {
      let files = target.files;
      if (files) {
        for (let file of files)
          this.addFile(file);

        await this.uploadFiles();
      }
    }

    const dropTarget = this.parentDrop ? this.parentElement : this;
    dropTarget.addEventListener('dragover', ev => ev.preventDefault());
    dropTarget.addEventListener('drop', async ev => await this.addDropFiles(ev));
  }

  selectFiles() {
    this.fileInput.click();
  }

  addFile(file) {
    if (this.files.find((f, idx) => f.name == file.name))
      return;
    const info = {
      fileName: file.name,
      displayName: file.name,
      mimeType: file.mimetype,
      size: file.size
    };
    this.filesInTransit.push(info);
    this.files.push(info);
  }

  async addDropFiles(ev) {
    // Prevent default behavior (Prevent file from being opened)
    ev.preventDefault();

    const dT = new DataTransfer();
    if (ev.dataTransfer.items) {
      // Use DataTransferItemList interface to access the file(s)
      for (const item of ev.dataTransfer.items) {
        // If dropped items aren't files, reject them
        if (item.kind === 'file') {
          const file = item.getAsFile();
          console.log('... file.name = ' + file.name);
          dT.items.add(file);
          this.addFile(file);
        }
      }
      this.fileInput.files = dT.files;
      await this.uploadFiles();
    } else {
      // Use DataTransfer interface to access the file(s)
      for (const file of ev.dataTransfer.files) {
        console.log('... file.name = ' + file.name);
        dT.items.add(file);
        this.addFile(file);
      }
      this.fileInput.files = dT.files;
      await this.uploadFiles();
    }
  }

  removeFile(fileName) {
    var index = this.files.findIndex((f) => f.name == fileName);
    if (index > -1)
      this.files.splice(index, 1);
  }

  reset() {
    this.files = [];
    this.filesInTransit = [];
    this.form.reset();
  }

  uploadFiles() {
    return new Promise((resolve, reject) => {
      let xhr = new XMLHttpRequest();
      xhr.open("POST", this.url);
      xhr.upload.addEventListener('loadstart', () => {
        this.dispatchEvent(new CustomEvent('start', { detail: { files: this.filesInTransit }}));
      });
      xhr.upload.addEventListener("progress", ({loaded, total}) => {
        const pct = Math.floor((loaded / total) * 100);
        this.dispatchEvent(new CustomEvent('progress', { detail: { percent: pct }}));
      });
      xhr.upload.addEventListener('error', () => {
        this.dispatchEvent(new CustomEvent('error', { detail: {reason: 'unknown error' }}));
      });
      xhr.upload.addEventListener('abort', () => {
        this.dispatchEvent(new CustomEvent('error', { detail: {reason: 'aborted' }}));
      });
      xhr.upload.addEventListener('timeout', () => {
        this.dispatchEvent(new CustomEvent('error', { detail: {reason: 'timeout' }}));
      });
      xhr.upload.addEventListener('loadend', () => {
        resolve(this.filesInTransit);
        this.filesInTransit = [];
        this.form.reset();
        this.dispatchEvent(new CustomEvent('end'));
      });
      xhr.onload = () => {
        // In local files, status is 0 upon success in Mozilla Firefox
        if (xhr.readyState === XMLHttpRequest.DONE) {
          const status = xhr.status;
          if (status >= 200 && status < 400) {
            // The request has been completed successfully
            this.dispatchEvent(new CustomEvent('uploaded', { detail: { response: JSON.parse(xhr.responseText) }}));
          }
        }
      };

      let data = new FormData(this.form);
      xhr.send(data);
    });
  }
}

customElements.define('ui-js-fileupload', UiJsFileupload);