import express from 'express'
import fileupload from 'express-fileupload'

const PORT = 8000;

function saveFile(file) {
  console.log(`upload ${file.name}`);
  return new Promise((resolve, reject) => {
      file.mv(`../uploads/${file.name}`, function(err) {
        if (err)
          reject(err);
        else
          resolve({
            name: file.name,
            mimetype: file.mimetype,
            size: file.size
          });
      });
  });
}

async function handleUploads(app) {
  app.post('/upload', async function(req, res) {
    console.log('upload started');
    if (!req.files || Object.keys(req.files).length === 0) {
      return res.status(400).send('No files were uploaded.');
    }

    let data = [];
    const files = Array.isArray(req.files.files) ? req.files.files : [req.files.files];

    for (const file of files) {
        try {
          const result = await saveFile(file);
          console.log(result);
          data.push(result);
        } catch (error) {
          return res.status(400).send(error);
        }
    }
    return res.send(data);
  });
}


const app = express();

app.use(express.static('../client'))
app.use('/lib', express.static('../lib'))

app.use(fileupload());
handleUploads(app);

app.listen(PORT, function() {
  console.log('Express server listening on port ', PORT); // eslint-disable-line
});
